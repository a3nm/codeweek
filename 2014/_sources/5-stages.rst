
=======================
Jour 5 – Stages de code
=======================

Programme ton ordi en Python !
::::::::::::::::::::::::::::::

Pour progresser en code, rien de tel qu'un stage pour rencontrer d'autres jeunes motivés.

Il y en a justement un organisé par les associations `Prologin <http://prologin.org>`_, `France-ioi <http://france-ioi.org>`_ et `Science ouverte <http://scienceouverte.fr>`_, **du 22 au 24 décembre 2014**, au Château de Ladoucette à Drancy (RER B). Le principe : résoudre des problèmes d'algorithmique par le code.

`Les inscriptions se font ici <http://scienceouverte.fr/Programme-ton-ordi-en-python>`_ pour les jeunes de **20 ans et moins** et sont limitées à 20 places.

Girls Can Code!
:::::::::::::::

Fin août, Prologin et France-ioi avaient déjà organisé un stage gratuit de programmation (Python ou C++) pour collégiennes et lycéennes. Au programme :

* résolution de problèmes en Python ou C++ via `la plate-forme de France-ioi <http://www.france-ioi.org/algo/>`_
* résolution d'énigmes sur tableau
* conférences sur divers thèmes :

  * le binaire
  * la théorie des jeux (ou comment gagner au jeu des allumettes de Fort Boyard)
  * le métier de développeuse (par `Stéphanie Hertrich <https://twitter.com/stepheUp>`_, Microsoft)
  * le métier d'expert en sécurité informatique

Quelques chiffres
=================

* 11 filles de 12 à 17 ans
* 7 jours
* 981 codes soumis
* 13779 lignes tapées
* 403 exercices résolus

Débrief
=======

.. image:: _static/groupe.jpg
    :align: center

Les filles étaient enchantées et nous aussi. Elles ont toutes voulu continuer à programmer mais seule la moitié a eu le temps / la motivation de résoudre résoudre des exercices sur le site. Presque toutes songent à l'informatique comme métier potentiel mais l'une d'elles a tenté d'en être dissuadée par son conseiller d'orientation : « Tu sais, il n'y a pas de métier dans l'informatique ». On en a tiré diverses leçons :

* il faut faire attention à bien chronométrer l'avancement de chacune parce que certaines participantes timides n'osent dire qu'elles sont bloquées et restent ainsi trop longtemps sur le même problème, à notre insu
* certaines filles cherchaient à faire le plus de problèmes possible, d'autres à faire des projets en groupe (notamment des jeux)
* quelques réactions :

    J'ai trouvé que [ce qu'a expliqué Stéphanie (de Microsoft)] était vraiment très intéressant, elle nous a permis de découvrir qu'avec l'informatique on peut faire plein de choses.

    Elle a pu nous démontrer que même les filles sont capables de vraiment faire leur place dans le milieu.
* les filles ne se rendaient pas compte des progrès vertigineux qu'elles avaient fait ; heureusement, Stéphanie leur a dit, alors qu'elle était surprise de leur capacité à comprendre le programme en C# qu'elle présentait : « Non mais là, je ne sais pas si vous vous rendez compte, mais vous venez de comprendre le code d'une autre personne écrit dans un autre langage, et ça c'est bien joué »
* (classique mais mérite d'être cité) certaines filles se sont rendu compte que la carrière de développeur était moins excitante que le stage
* un message des parents :

    Merci beaucoup pour ce stage hyper bien organisé. Vic est rentrée gonflée à bloc. Elle a eu la confirmation que c'est ce qu'elle veut faire dans la suite de ses études. J'en suis ravie et soulagée. Merci à toute votre équipe. Bonne continuation.

Bon, certes, pendant ce temps, une association humanitaire d'étudiants de l'ESSEC construisait `2 aires de jeu en Afrique du Sud <https://www.youtube.com/watch?v=aJOLIk7hdX4>`_.

Algoréa
:::::::

France-ioi organise également un stage d'algorithmique de haut niveau intitulé `Algoréa <http://www.algorea.org>`_.

Celui-ci est également entièrement gratuit mais la sélection se fait par le biais d'un concours. Les épreuves se déroulent en février 2015, mais il est déjà temps de `s'entraîner <http://www.france-ioi.org/algo/>`_ !

.. raw:: html
    
    <div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = 'jilljenn';
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
