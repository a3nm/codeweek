
==========================================
Jour 7 – Comment est codé JJ's Code Week ?
==========================================

J'aimerais parler de la plate-forme avec laquelle j'ai réalisé mes billets pour cette semaine de l'enseignement de l'informatique. Mais avant, je vais mentionner un outil similaire.

Notebooks IPython
:::::::::::::::::

Si vous cherchez à publier sur le Web des documents contenant texte, images, maths et code, les notebooks IPython sont un moyen de le faire :

- `Un exemple de document IPython <http://nbviewer.ipython.org/url/cs.marlboro.edu/courses/spring2014/information/code/dct/dct.ipynb>`_
- `Jouons avec les données publiques des Vélib' <http://nbviewer.ipython.org/gist/rossant/5520933>`_
- `Peter Norvig à propos du comic XKCD « Regex Golf » <http://nbviewer.ipython.org/url/norvig.com/ipython/xkcd1313-part2.ipynb?create=1>`_
- `Une galerie de notebooks IPython qui valent le coup d'œil <https://github.com/ipython/ipython/wiki/A-gallery-of-interesting-IPython-Notebooks>`_

En fait, l'auteur du deuxième exemple ci-dessus, Cyrille Rossant (`@cyrillerossant <https://twitter.com/cyrillerossant>`_) a même écrit `deux livres open source <http://cyrille.rossant.net/books/#minibook>`_ sur l'utilisation de IPython pour l'analyse de données (que vous pouvez quand même `acheter au format papier une douzaine d'euros <https://www.packtpub.com/big-data-and-business-intelligence/ipython-interactive-computing-and-visualization-cookbook>`_ pour remercier l'auteur). Deux extraits :

- `Introduction to Machine Learning in Python with scikit-learn <http://ipython-books.github.io/featured-04/>`_
- `Introduction to statistical data analysis in Python – frequentist and Bayesian methods <http://ipython-books.github.io/featured-07/>`_

En novembre 2014, IPython est même apparu dans *Nature* (`Interactive notebooks: Sharing the code <http://www.nature.com/news/interactive-notebooks-sharing-the-code-1.16261>`_) :

    Designed to make data analysis easier to share and reproduce, the IPython notebook is being used increasingly by scientists who want to keep detailed records of their work, devise teaching modules and collaborate with others. Some researchers are even publishing the notebooks to back up their research papers — and Brown, among others, is pushing to use the program as a new form of interactive science publishing.

Des cours en ligne avec RuneStone
:::::::::::::::::::::::::::::::::

L'article que vous êtes actuellement en train de lire a été rédigé sur `RuneStone <http://runestoneinteractive.org>`_, une plate-forme développée par Brad Miller et David Ranum (Luther College, Iowa). RuneStone permet de rédiger des cours comportant des fenêtres interactives où l'on peut exécuter du code Python. L'intérêt : pouvoir directement tester et modifier les scripts associés à une leçon.

Le code ci-dessous permet de tracer une pyramide de « o ». Cliquez sur « Run », puis modifiez la valeur de la variable ``taille`` et relancez « Run ». 

.. activecode:: pyramide

    taille = 5
    for i in range(taille):
        print('o' * (i + 1))

On peut même suivre pas à pas le contenu des variables du script.

.. codelens:: debugger

    taille = 5
    for i in range(taille):
        print('o' * (i + 1))

Et on peut faire `plein d'autres choses <http://interactivepython.org/runestone/static/overview/overview.html>`_ (incorporer des QCM aux leçons, par exemple).

Par exemple, le code complet de cet article est accessible `ici <_sources/7-interactive-python.txt>`_.

Bifurquez-moi sur Bitbucket
:::::::::::::::::::::::::::

(Si vous avez une meilleure traduction de « Fork me on Bitbucket », je suis preneur.)

Pour récupérer l'entièreté du code de la JJ's Code Week, je vous invite à installer Git et virtualenv, puis à entrer les commandes suivantes :

.. code-block:: bash

    $ git clone https://bitbucket.org/jilljenn/codeweek.git
    $ pip install -r requirements.txt
    $ virtualenv .
    $ cd 2014
    $ paver build
    $ cd build
    $ python3 -m http.server

Et vous devriez avoir accès aux pages à l'adresse http://localhost:8000.

Merci pour votre attention durant cette semaine ! N'hésitez pas à m'envoyer vos remarques par mail à l'adresse vie@jill-jenn.net.

.. raw:: html
    
    <div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = 'jilljenn';
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
