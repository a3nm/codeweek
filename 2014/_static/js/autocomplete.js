var videos = new Bloodhound({
    datumTokenizer: function(d) { return d.tokens; },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: '_static/cdf.json'
});

videos.initialize();

$('.typeahead').typeahead(null, {
    name: 'videos',
    source: videos.ttAdapter(),
    templates: {
        suggestion: Handlebars.compile([
            '<p class="repo-language">{{date}}</p>',
            '<p class="repo-name">{{value}}</p>',
            '<p class="repo-description">{{lecturer}}</p>'
        ].join(''))
    }
});

$('input.typeahead').on('typeahead:selected', function(event, selection) {
  $('#result').attr('href', selection.url);
  $('#result').html('Accéder à la vidéo <em>' + selection.value + '</em> par ' + selection.lecturer);
}).on('typeahead:autocompleted', function(event, selection) {
  $('#result').attr('href', selection.url);
  $('#result').html('Accéder à la vidéo <em>' + selection.value + '</em> par ' + selection.lecturer);
});
